import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-tables-multiplication',
  templateUrl: './tables-multiplication.component.html',
  styleUrls: ['./tables-multiplication.component.scss']
})
export class TablesMultiplicationComponent implements OnInit {
  identForm!: FormGroup;
  isSubmitted = false;
  badLogin = false;
  multipli = 0;
  tab_mult= [{num: 1},{num: 2},{num: 3},{num: 4},{num: 5},{num: 6},{num: 7},{num: 8},{num: 9},{num: 10}];
  nbTable = Array();

  constructor() { }

  ngOnInit(): void {
    this.identForm = new FormGroup({
      multipli: new FormControl()
    });

  }
  get formControls() { return this.identForm.controls; }
  mult2() {
    this.multipli = this.identForm.get('multipli')?.value;
    console.log();
    this.isSubmitted = true;
    console.log("number : " + this.identForm.value);
    if (this.identForm.value.multipli == '') {
      this.badLogin = true;
      return;
    } else {
      this.nbTable=[];
      const number = this.identForm.value.multipli;
      for (let j = 1; j <= number; j++) {
        this.nbTable.push(j);
        console.log(this.nbTable);

        for (let i = 1; i <= 10; i++) {
          const result = i * j;
          console.log(`${j} * ${i} = ${result}`);
          console.log(j);

        }
      }
    }
  }
}
